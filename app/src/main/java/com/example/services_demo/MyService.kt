package com.example.services_demo

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*

class MyService : Service() {

    private  var mHandler: Messenger? = null

    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "srvice strted", Toast.LENGTH_SHORT).show()

        mHandler = intent?.getParcelableExtra<Messenger>("handler")
        val input = intent!!.getStringExtra("inputExtra")
        createNotificationChannel()
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this,0,
            notificationIntent, 0)

        /**code for foreground service*/
        val notification = NotificationCompat.Builder(this, "hello")
            .setContentText(input)
            .setSmallIcon(R.drawable.ic_baseline_notifications_active_24)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notification)

        /**Location fetching code below*/
        val fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(this)
        val locationRequest =
            LocationRequest().setInterval(5000).setFastestInterval(5000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        fusedLocationClient.requestLocationUpdates(
            locationRequest, object : LocationCallback() {
                override fun onLocationResult(p0: LocationResult?) {
                    super.onLocationResult(p0)

                    /**code for getting latlong from locationResult*/
                    for (location in p0!!.locations) {
                        Log.e("latitude", "$location.latitude.toString()")
                        Log.e("longitude", "$location.longitude.toString()")

                        val lati = location.latitude.toString()
                        val longi = location.longitude.toString()

                        /**sending data to main activity*/
                        /*val i = Intent()
                        i.action = "location"
                        i.putExtra("latitude", lati)
                        i.putExtra("longitude", longi)
                       sendBroadcast(i)*/

                        /**code for thread to pass data to the activity*/
                        val msg = Message()
                        val b = Bundle()
                        val value1 = lati
                        val value2 = longi
                        b.putString("KEY1", value1)
                        b.putString("KEY2", value2)
                        msg.data = b
                        mHandler?.send(msg)
                    }
                }
            }, Looper.myLooper())
        return START_NOT_STICKY
    }

    /**notification chanl for services*/
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel("hello","Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "srvice dstryd", Toast.LENGTH_SHORT).show()
    }
}