package com.example.services_demo

import android.app.ActivityManager
import android.app.Service
import android.content.*
import android.os.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    private lateinit var mRunnable: Runnable
    var boundService: BindService? = null
    var isBound = false
    val handler = Handler()
    val handlr : Handler = object : Handler(Looper.myLooper()!!){
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            val b = msg.data
            val latti = b.getString("KEY1")
            val longii = b.getString("KEY2")
            txt_latitude.setText(latti)
            txt_longitude.setText(longii)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val serviceClass = BindService::class.java
        val intent = Intent(applicationContext, serviceClass)

        /**code to receive data from handler coming from service class*/

        intent.putExtra("handler",Messenger(handlr))

        /**button click listener*/
        btn_start.setOnClickListener {
            if (isServiceRunning(serviceClass) == false) {
                startService(intent)
                bindService(intent , boundServiceConnection,BIND_AUTO_CREATE);
            } else {
                Toast.makeText(this, "alrdy running", Toast.LENGTH_SHORT).show()
            }
        }

        btn_stop.setOnClickListener {

            if (isServiceRunning(serviceClass) == true) {
                stopService(intent)
            } else {
                Toast.makeText(this, "alrdy stoped", Toast.LENGTH_SHORT).show()
            }
        }

        btn_status.setOnClickListener {
            if (isServiceRunning(serviceClass) == true) {
                Toast.makeText(this, "Service Running", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Service Stoped", Toast.LENGTH_SHORT).show()
            }
        }
        btn_bind.setOnClickListener {
            boundService?.locationFetch()
        }
    }

    val broadcastReceiver =object: BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            if(p1!!.action=="location"){
                if(txt_latitude!=null) {
                    txt_latitude.setText(p1.getStringExtra("latitude"))
                    txt_longitude.setText(p1.getStringExtra("longitude"))
                }
            }
        }
    }

    private fun isServiceRunning(serviceClass: Class<BindService>): Any {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in activityManager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()

    }


    override fun onResume() {
        super.onResume()
        registerReceiver(broadcastReceiver, IntentFilter("location"))
    }

    override fun onStop() {
        super.onStop()
        if(isBound){
            unbindService(boundServiceConnection)
            isBound = false
        }
    }
    var boundServiceConnection: ServiceConnection = object: ServiceConnection{
        override fun onServiceDisconnected(p0: ComponentName?) {
            isBound = false
            boundService= null
        }

        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {

            val binderBridge= p1 as BindService.MyBinder
            boundService = binderBridge.service
            isBound = true
        }

    }
}